FROM alpine
ARG JENKINS=/jenkins-0.0.1-SNAPSHOT.jar
COPY ${JENKINS} jenkins-service.jar  
RUN apk add openjdk11
ENV JAVA_HOME /usr/lib/jvm/jdk-11.0.9
CMD ["java", "-jar", "jenkins-service.jar"]
EXPOSE 23080