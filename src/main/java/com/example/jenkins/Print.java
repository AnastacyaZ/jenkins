package com.example.jenkins;

import org.springframework.stereotype.Service;

@Service
public class Print {

    public static void printHello() {
        System.out.println("Hello");
    }

    public static void printProj() {
        System.out.println("from Jenkins project !!!");
    }

    public static void printCommitHook() {
        System.out.println("This is a commit hook !!!");
    }
}
